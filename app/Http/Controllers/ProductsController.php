<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    // MÉTODO PARA DAR DE ALTA NUEVOS PRODUCTOS
    public function create(SaveProductRequest $request){
        Product::create($request->validated());

        return redirect()->route('create');
    }
    // MÉTODO PARA MOSTRAR UN LISTADO DE TODOS LOS PRODUCTOS
    public function read(){

        $productos=Product::paginate(4);
        return view('read', compact('productos'));
    }
    //MÉTODO PARA MOSTRAR EL FORMULARIO PARA EDITAR UN PRODUCTO
    public function edit(Product $producto){
        return view('edit', compact('producto'));
    }

    //MÉTODO PARA ACTUALIZAR LOS DATOS DE UN PRODUCTO
    public function update(Product $producto, SaveProductRequest $request){
        $producto->update($request->validated());
        return redirect()->route('read');
    }
    public function delete(Product $producto){
        $producto->delete();
        return redirect()->route('read');
    }
}
