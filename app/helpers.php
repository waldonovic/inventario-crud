<?php

function setActive($routeName){
    return request()->routeIs($routeName) ? 'active' : '';
}
function oldSelect($nameField,$value){
    return old($nameField) == $value ? 'selected' : '' ;
}
function chooseSelect($saveSelect,$optionSelect){
    if($saveSelect==$optionSelect){
        return "selected='selected'";
    }
}
?>