<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//RUTA PARA DAR LA BIENVENIDA
Route::view('/', 'home')->name('home');

//RUTA PARA DAR DE ALTA LOS PRODUCTOS
Route::view('/create', 'create')->name('create');
Route::post('create', 'ProductsController@create');

//RUTA PARA VER LOS PRODUCTOS
Route::get('/read','ProductsController@read')->name('read');

//RUTA PARA EDITAR LOS PRODUCTOS
Route::get('/edit/{producto}', 'ProductsController@edit')->name('edit');

//RUTA PARA GUARDAR LOS CAMBIOS DE LOS PRODUCTOS
Route::patch('/update/{producto}', 'ProductsController@update')->name('update');

//RUTA PARA ELIMINAR LOS PRODUCTOS
Route::delete('/delete/{producto}','ProductsController@delete')->name('delete');
