@extends('layout')

@section('title', 'Creación')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-lg-10 mx-auto">
                <form class="bg-white shadow rounded py-3 px-4" method="POST" action="{{ route('create') }}">
                    @csrf
                    <h1 class="display-4">Alta de productos</h1>
                    <hr>
                    <div class="form-group">
                        <label for="name">Nombre del producto:</label>
                        <input id="name" class="form-control bg-light shadow-sm @error('name') is-invalid @else border-0 @enderror" name="name" 
                        placeholder="Nombre del producto" value="{{ old('name') }}" >
                        @error('name')
                            <span class="invalid-feedbak" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category">Categoria del producto:</label>
                        <select id="category" class="form-control bg-light shadow-sm" name="category" value="{{ old('category') }}">
                            <option value="Sin categoria" {{ oldSelect('category','Sin categoria') }}>Sin categoria</option>
                            <option value="Productos Frescos" {{ oldSelect('category','Productos Frescos') }}>Productos Frescos</option>
                            <option value="Carne" {{ oldSelect('category','Carne') }}>Carne</option>
                            <option value="Lacteos" {{ oldSelect('category','Lacteos') }}>Lacteos</option>
                            <option value="Alimentos Congelados" {{ oldSelect('category','Alimentos Congelados') }}>Alimentos Congelados</option>
                            <option value="Pan, Cereal, Arroz y Pastas" {{ oldSelect('category','Pan, Cereal, Arroz y Pastas') }}>Pan, Cereal, Arroz y Pastas</option>
                            <option value="Alimentos Básicos y Miseláneos" {{ oldSelect('category','Alimentos Básicos y Miseláneos') }}>Alimentos Básicos y Miseláneos</option>
                            <option value="Salud y Belleza" {{ oldSelect('category','Salud y Belleza') }}>Salud y Belleza</option>
                            <option value="Articulos para el Hogar" {{ oldSelect('category','Articulos para el Hogar') }}>Articulos para el Hogar</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="trademark">Marca comercial del producto:</label>
                        <input class="form-control bg-light shadow-sm @error('trademark') is-invalid @else border-0 @enderror" id="trademark" name="trademark" placeholder="Marca comercial" value="{{ old('trademark') }}"><br>
                        @error('trademark')
                            <span class="invalid-feedbak" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="quantity">Cantidad de piezas o kilogramos:</label>
                        <input class="form-control bg-light shadow-sm @error('quantity') is-invalid @else border-0 @enderror" type="number" id="quantity" name="quantity" value="{{ old('quantity') }}"><br>
                        @error('quantity')
                            <span class="invalid-feedbak" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="purchase">Precio a la compra del producto:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">$</span>
                            </div>
                            <input class="form-control bg-light shadow-sm @error('purchase') is-invalid @else border-0 @enderror" type="number" step="any" id="purchase" name="purchase" value="{{ old('purchase') }}" aria-describedby="basic-addon1"><br>
                            @error('purchase')
                                <span class="invalid-feedbak" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sale">Precio a la venta del producto:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon2">$</span>
                            </div>
                            <input class="form-control bg-light shadow-sm @error('sale') is-invalid @else border-0 @enderror" type="number" step="any" id="sale" name="sale" value="{{ old('number') }}" aria-describedby="basic-addon2"><br>
                            @error('sale')
                                <span class="invalid-feedbak" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
        
                        <button class="btn btn-primary btn-block">Guardar</button>
                </form>
            </div>
        </div>      
        
    </div>

@endsection