@extends('layout')

@section('title', 'Visualización')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-lg-10 mx-auto">
                <h1>Listado de productos</h1>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Marca Comercial</th>
                            <th scope="col">Precio a la compra</th>
                            <th scope="col">Precio a la venta</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($productos as $producto)
                            <tr>
                                <td>{{ $producto->name }}</td>
                                <td>{{ $producto->category }}</td>
                                <td>{{ $producto->trademark }}</td>
                                <td>$ {{ $producto->purchase }}</td>
                                <td>$ {{ $producto->sale }}</td>
                                <td class="d-flex justify-content-between">
                                    <a class="btn btn-success text-white" href="{{route('edit',$producto->id)}}"><i class="fas fa-pencil-alt"></i></a> 
                                    <form method="POST" action="{{route('delete',$producto)}}">
                                        @csrf @method('DELETE')
                                        <button class="btn btn-danger text-white"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h3>No hay productos para mostrar</h3>
                        @endforelse
                    </tbody>
                </table>
                {{ $productos->links() }}
            </div>
        </div>
    </div>
@endsection