@extends('layout')

@section('title', 'Bienvenida')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-10 col-lg-10 mx-auto">
            <h1 class="display-4">Inventario para todos</h1>
            <hr>
            <img src="{{asset('images/inventario.jpg')}}" alt="inventario" class="img-thumbnail h-75 w-60">
        </div>
    </div>
</div>

@endsection