<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <script src="{{mix('js/app.js')}}" defer></script>
    <title>@yield('title')</title>
</head>
<body>
    <div id="app" class="d-flex flex-column h-screen justify-content-between">
        <header>
            @include('partials.nav')
        </header>
        <main class="py-4">
            @yield('content')
        </main>
        <footer class="bg-white  text-black-50 text-center py-3 shadow">
            {{ config('app.name') }}| Copyright &#9400 {{ date('Y') }}
        </footer>
    </div>
</body>
</html>